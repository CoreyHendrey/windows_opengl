#include "Shader.h"

	Shader::Shader(const GLchar* vertexPath, const GLchar* fragmentPath)
	{
		std::string vertexCode;
		std::string fragmentCode;

		std::ifstream vShaderFile;
		std::ifstream fShaderFile;

		vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

		try
		{
			vShaderFile.open(vertexPath);
			fShaderFile.open(fragmentPath);
			std::stringstream vShaderStream, fShaderStream;
			
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();

			vShaderFile.close();
			fShaderFile.close();

			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();

		}
		catch (std::ifstream::failure e)
		{
			std::cout << "ERROR:SHADER::FILE_NOT_READ\n";
		}

		const char* vShaderCode = vertexCode.c_str();
		const char* fShaderCode = fragmentCode.c_str();

		unsigned int vertex, fragment;

		vertex = createShader(GL_VERTEX_SHADER, vShaderCode);
		fragment = createShader(GL_FRAGMENT_SHADER, fShaderCode);

		ID = glCreateProgram();
		glAttachShader(ID, vertex);
		glAttachShader(ID, fragment);
		glLinkProgram(ID);

		int success;
		char infoLog[512];
		glad_glGetProgramiv(ID, GL_LINK_STATUS, &success);
		if(!success)
		{
			glGetProgramInfoLog(ID, 512, NULL, infoLog);
			std::cout << "Linking failed: " << infoLog << "\n";
		}

		glDeleteShader(vertex);
		glDeleteShader(fragment);

	}

	unsigned int Shader::createShader(unsigned int shaderType, const char* source)
	{
		unsigned int shader;
		shader = glCreateShader(shaderType);
		glShaderSource(shader, 1, &source, NULL);
		glCompileShader(shader);

		int success;
		char infoLog[512];
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

		if (!success)
		{
			glGetShaderInfoLog(shader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::COMPILATION_FAILED\n" << infoLog << std::endl;
		}

		return shader;
	}

	void Shader::use()
	{
		glUseProgram(ID);
	}

	void Shader::setBool(const std::string &name, bool value) const
	{
		glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
	}

	void Shader::setInt(const std::string &name, int value) const
	{
		glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
	}

	void Shader::setFloat(const std::string &name, float value) const
	{
		glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
	}

	void Shader::setVec3(const std::string &name, glm::vec3 values) const
	{
		glUniform3f(glGetUniformLocation(ID, name.c_str()), values[0], values[1], values[2]);
	}

	void Shader::setVec4(const std::string &name, glm::vec4 values) const
	{
		glUniform4f(glGetUniformLocation(ID, name.c_str()), values[0], values[1], values[2], values[3]);
	}

	void Shader::setMat4fv(const std::string& name, glm::mat4 matrix) const
	{
		glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, glm::value_ptr(matrix));
	}

	void Shader::setMaterial(Material mat) const
	{
		//setVec3("material.ambient", mat.ambient);
		//setVec3("material.diffuse", mat.diffuse);
		//setVec3("material.specular", mat.specular);
		//setFloat("material.shininess", mat.shininess);
	}

	void Shader::setPointLight(int id, PointLight l)
	{
		std::string lstr = "lights[" + std::to_string(id) + "].";
		setVec3(lstr + "ambient", l.m_ambient);
		setVec3(lstr + "diffuse", l.m_diffuse);
		setVec3(lstr + "position", l.m_position);
		setVec3(lstr + "specular", l.m_specular);
		setFloat(lstr + "shininess", l.m_shininess);

		setFloat(lstr + "constant", l.m_constant);
		setFloat(lstr + "linear", l.m_linear);
		setFloat(lstr + "quadratic", l.m_quadratic);
	}
