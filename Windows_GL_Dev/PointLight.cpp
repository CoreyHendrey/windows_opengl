#include "PointLight.h"

PointLight::PointLight(
	glm::vec3 position, 
	glm::vec3 ambient, 
	glm::vec3 diffuse, 
	glm::vec3 specular, 
	float shininess, 
	float constant, 
	float linear, 
	float quadratic)
{
	m_position = position;
	m_ambient = ambient;
	m_diffuse = diffuse;
	m_specular = specular;
	m_shininess = shininess;
	m_constant = constant;
	m_linear = linear;
	m_quadratic = quadratic;
}
