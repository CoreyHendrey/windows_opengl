#version 450 core 
struct DirectionalLight {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

#define NUM_DIR_LIGHTS 1
uniform DirectionalLight dirLights[NUM_DIR_LIGHTS];

struct PointLight {
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

#define NUM_POINT_LIGHTS 4
uniform PointLight lights[NUM_POINT_LIGHTS];

struct Material {
	sampler2D diffuse;
	sampler2D specular;
	sampler2D emission;
	float emissiveness;
	float shininess;
};

out vec4 fColor;

in vec2 texCoord;
in vec3 fragPos;
in vec3 normal;

uniform Material material;
uniform vec3 viewPos;

vec3 pointLightCalc(PointLight light, vec3 norm, vec3 viewDir, vec3 fragPos);
vec3 directLightCalc(DirectionalLight light, vec3 norm, vec3 viewDir);

void main() 
{ 
	vec3 outputColor = vec3(0, 0, 0);
	vec3 norm = normalize(normal);
	vec3 viewDir = normalize(viewPos - fragPos);
	
	vec3 emission = vec3(texture(material.emission, texCoord)) * material.emissiveness;

	for(int i = 0; i < NUM_POINT_LIGHTS; i++)
	{
		outputColor += pointLightCalc(lights[i], norm, viewDir, fragPos);
	}

	vec4 result = vec4(outputColor, 1.0f);
	fColor = result;
}

vec3 pointLightCalc(PointLight light, vec3 norm, vec3 viewDir, vec3 fragPos)
{
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	vec3 lightDir = normalize(light.position - fragPos);
	vec3 halfDir = normalize(lightDir + viewDir);

	//Direction vector from our view to fragPos
	//Specular
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(norm, halfDir), 0.0f), material.shininess);
	vec3 specular = (vec3(texture(material.specular, texCoord)) * spec) * light.specular;

	//Ambient
	vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord)); 

	//Diffuse
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * light.diffuse  * vec3(texture(material.diffuse, texCoord)); 

	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	return ambient + diffuse + specular;
}

vec3 directLightCalc(DirectionalLight light, vec3 norm, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	//Direction vector from our view to fragPos
	//Specular
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
	vec3 specular = (vec3(texture(material.specular, texCoord)) * spec) * light.specular;

	//Ambient
	vec3 ambient = light.ambient * vec3(texture(material.diffuse, texCoord)); 

	//Diffuse
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * light.diffuse  * vec3(texture(material.diffuse, texCoord)); 

	return ambient + diffuse + specular;
}