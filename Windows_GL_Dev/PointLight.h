#pragma once
#include <glm/glm.hpp>

class PointLight
{
	public:
		glm::vec3 m_position;
		glm::vec3 m_ambient;
		glm::vec3 m_diffuse;
		glm::vec3 m_specular;
		float m_shininess;
		float m_constant;
		float m_linear;
		float m_quadratic;

		PointLight(
			glm::vec3 position, 
			glm::vec3 ambient, 
			glm::vec3 diffuse, 
			glm::vec3 specular, 
			float shininess, 
			float constant,
			float linear, 
			float quadratic);
};

