#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform vec4 offset;

out vec3 ourColor;
out vec4 pos;

void main()
{
	gl_Position = vec4(position.x + offset.x, -position.y + offset.y, position.x, 1.0f);
	pos = gl_Position;
	ourColor = color;
}

