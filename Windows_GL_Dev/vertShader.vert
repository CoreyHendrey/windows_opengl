#version 450 core

layout (location = 0) in vec3 pos;	
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

layout (std140) uniform Matrices
{
	uniform mat4 projection;	//0				//0  Col 1
												//16 Col 2
												//32 Col 3
												//48 Col 4
	uniform mat4 view;		//64				//64 Col 1
												//80 Col 2
												//96 Col 3
												//112 Col 4
};

uniform mat4 model;

out vec2 texCoord;
out vec3 normal;
out vec3 fragPos;

void main()	
{ 
	gl_Position = projection * view * model * vec4(pos, 1.0f);
	//ourColor = vec3(gl_Position.xyz + 0.5f);							
	fragPos = vec3(model * vec4(pos, 1.0));
	texCoord = aTexCoord;
	normal = mat3(transpose(inverse(model))) * aNormal;
}	