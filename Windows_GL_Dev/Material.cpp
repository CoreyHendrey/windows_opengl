#include "Material.h"

Material::Material(const char* diffusePath, const char* specularPath)
{
	m_diffuse = diffusePath;
	m_specular = specularPath;
}