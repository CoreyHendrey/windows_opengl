#pragma once
#include "Material.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>
#include "stb_image.h"

class Material
{
public:
	unsigned int diffuse;
	unsigned int specular;
	const char* m_diffuse;
	const char* m_specular;
	Material(const char* diffusePath, const char* specularPath);
};

