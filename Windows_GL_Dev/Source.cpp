#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Shader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Source.h"

bool fly = true;

const float SCREEN_WIDTH = 800;
const float SCREEN_HEIGHT = 600;

glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraDirection = glm::normalize(cameraPos - cameraTarget);
//Y up
glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
glm::vec3 forward = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 right = glm::vec3(0.0f, 1.0f, 0.0f);
glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));
glm::vec3 cameraUp = glm::cross(cameraDirection, cameraRight);

double yaw = -90;
double pitch = 0;
double cameraSensitivity = 1.5f;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

double lastX = SCREEN_WIDTH / 2.0;
double lastY = SCREEN_HEIGHT / 2.0;

//This is called when an error is thrown
void error_callback(int error, const char* description)
{
	puts(description);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window)
{
	float speed = 2.0f * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		cameraPos += speed * cameraFront;
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		cameraPos -=  speed * cameraFront;
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		cameraPos -= speed * glm::normalize(glm::cross(cameraFront, up));
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		cameraPos += speed * glm::normalize(glm::cross(cameraFront, up));
	}

	if(!fly)
		cameraPos.y = 0.0f;

}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	double deltaX = xpos - lastX;
	double deltaY = ypos - lastY;

	yaw += deltaX * deltaTime * cameraSensitivity;
	pitch -= deltaY * deltaTime * cameraSensitivity;

	if (pitch < -90) pitch = 90;
	if (pitch > 90) pitch = 90;

	cameraFront.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
	cameraFront.y = sin(glm::radians(pitch));
	cameraFront.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
	cameraFront = glm::normalize(cameraFront);
}

int main(void)
{
	if (!glfwInit())
	{
		std::cout << "Could not initialize GLFW";
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwSetErrorCallback(error_callback);

	GLFWwindow* window = glfwCreateWindow(800, 600, "Window!", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create window\n";
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD\n";
		return -1;
	}

	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(window, mouse_callback);

	stbi_set_flip_vertically_on_load(true);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	//Enable GAMMA correction
	glEnable(GL_FRAMEBUFFER_SRGB);

	//Creates a triangle vertex data
	float vertices[] =
	{
		//Back
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,	  0.0f,  0.0f, -1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,	  0.0f,  0.0f, -1.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,	 0.0f,  0.0f, -1.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,	 0.0f,  0.0f, -1.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f,	 0.0f,  0.0f, -1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,	  0.0f,  0.0f, -1.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,	  0.0f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,	 0.0f,  0.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f,	 0.0f,  0.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f,	 0.0f,  0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,	  0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,	  0.0f,  0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,	 -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,	 -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,	 -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,	 -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,	 -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,	 -1.0f,  0.0f,  0.0f,

		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,	 1.0f,  0.0f,  0.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 0.0f,	 1.0f,  0.0f,  0.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,	 1.0f,  0.0f,  0.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,	 1.0f,  0.0f,  0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,	 1.0f,  0.0f,  0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,	 1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,	  0.0f, -1.0f,  0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f,	 0.0f, -1.0f,  0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,	 0.0f, -1.0f,  0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,	 0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,	  0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,	  0.0f, -1.0f,  0.0f,

		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,	  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,	  0.0f,  1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,	 0.0f,  1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,	 0.0f,  1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,	 0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,	  0.0f,  1.0f,  0.0f
	};


	const unsigned int worldXsize = 10;
	const unsigned int worldYsize = 4;
	const unsigned int worldZsize = 10;

	unsigned int index = 0;
	glm::vec3 cubePos[worldXsize * worldYsize * worldZsize];

	for (unsigned int y = 0; y < worldYsize; y++)
	{
		for (unsigned int x = 0; x < worldXsize; x++)
			for (unsigned int z = 0; z < worldZsize; z++)
			{
				cubePos[index] = glm::vec3(x * rand() % 10, y * rand() % 10, z * rand() % 10);
				index++;
			}
	}

	//const unsigned int NUM_DIR_LIGHTS = 1;
	//glm::vec3 dirLightDirection[NUM_DIR_LIGHTS];
	//for (unsigned int i = 0; i < NUM_DIR_LIGHTS; i++)
	//{
	//	dirLightDirection[i] = glm::vec3(-0.2f, -1.0f, -0.3f);
	//}

	Shader shader("vertShader.vert", "fragShader.frag");
	Shader lightShader("vertShader.vert", "lightFragShader.frag");

	glm::vec3 ambient = glm::vec3(0.0f, 0.5f, 0.31f);
	glm::vec3 diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::vec3 specular = glm::vec3(0.25f, 0.25f, 0.25f);

	//WIREFRAME
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Create Buffers
	unsigned int VBO;
	unsigned int VAO;
	unsigned int lightVAO;
	//unsigned int EBO;

	glGenBuffers(1, &VBO);
	//glGenBuffers(1, &EBO);
	glGenVertexArrays(1, &VAO);

	//Now we are bound to this VAO
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) (sizeof(float) * 3));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*) (sizeof(float) * 5));
	glEnableVertexAttribArray(2);

	glGenVertexArrays(1, &lightVAO);
	glBindVertexArray(lightVAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


	unsigned int matrices_index_shader = glGetUniformBlockIndex(shader.ID, "Matrices");
	unsigned int matrices_index_light = glGetUniformBlockIndex(lightShader.ID, "Matrices");
	glUniformBlockBinding(shader.ID, matrices_index_shader, 0);
	glUniformBlockBinding(lightShader.ID, matrices_index_light, 0);

	unsigned int UBO;
	glGenBuffers(1, &UBO);
	glBindBuffer(GL_UNIFORM_BUFFER, UBO);
	glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	glBindBufferRange(GL_UNIFORM_BUFFER, 0, UBO, 0, 2 * sizeof(glm::mat4));

	glm::mat4 projection;
	projection = glm::perspective(glm::radians(45.0f), SCREEN_WIDTH / SCREEN_HEIGHT, 0.1f, 100.0f);

	glBindBuffer(GL_UNIFORM_BUFFER, UBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(projection));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	Material material = Material("Textures/container2.png", "Textures/container2_specular.png");
	loadTexture(material.diffuse, material.m_diffuse);
	loadTexture(material.specular, material.m_specular);

	glm::vec3 lambient = glm::vec3(0.05f, 0.05f, 0.1f);
	glm::vec3 lspecular = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::vec3 ldiffuse = glm::vec3(0.5f, 0.5f, 1.0f);

	PointLight pointLights[4] = 
	{
		PointLight(glm::vec3(0.0f, 0.0f, 0.0f), lambient, ldiffuse, lspecular, 32.0f, 1.0f, 0.09f, 0.032f),
		PointLight(glm::vec3(0.0f, 0.0f, 0.0f), lambient, ldiffuse, lspecular, 32.0f, 1.0f, 0.09f, 0.032f),
		PointLight(glm::vec3(0.0f, 0.0f, 0.0f), lambient, ldiffuse, lspecular, 32.0f, 1.0f, 0.09f, 0.032f),
		PointLight(glm::vec3(0.0f, 0.0f, 0.0f), lambient, ldiffuse, lspecular, 32.0f, 1.0f, 0.09f, 0.032f)
	};

	

	//This starts the render loop
	//Without it the window would instantly close
	//glfwWindowShouldClose will return true when the window is closed
	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		glClearColor(0.0f, 0.0f, 0.001f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		float timeValue = glfwGetTime();

		glm::mat4 view;
		view = glm::lookAt(cameraPos, cameraPos + cameraFront, up);

		pointLights[0].m_position = glm::vec3(sin(timeValue), cos(timeValue), 0.0f) * 9.0f + 5.0f;
		pointLights[1].m_position = glm::vec3(-sin(timeValue), -cos(timeValue), 0.0f) * 9.0f + 5.0f;
		pointLights[2].m_position = glm::vec3(sin(timeValue), 0.0f, cos(timeValue)) * 9.0f + 5.0f;
		pointLights[3].m_position = glm::vec3(-sin(timeValue), 0.0f, -cos(timeValue)) * 9.0f + 5.0f;

		glBindBuffer(GL_UNIFORM_BUFFER, UBO);
		glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		//Create light models
		lightShader.use();

		glBindVertexArray(lightVAO);
		for (unsigned int i = 0; i < 4; i++)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, pointLights[i].m_position);
			lightShader.setVec3("lightColor", pointLights[i].m_diffuse);
			lightShader.setMat4fv("model", model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		glBindVertexArray(VAO);
		shader.use();

		//Activating a material
		shader.setFloat("material.shininess", 64.0f);
		shader.setInt("material.diffuse", 0);
		shader.setInt("material.specular", 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, material.diffuse);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, material.specular);
		
		for (unsigned int i = 0; i < worldXsize * worldYsize * worldZsize; i++)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, cubePos[i]);
			shader.setMat4fv("model", model);
			shader.setVec3("viewPos", cameraPos);

			//Loop through lights to apply lighting
			//I imagine this is where culling would happen
			for (unsigned int i = 0; i < 4; i++)
				shader.setPointLight(i, pointLights[i]);

			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		glfwSwapBuffers(window);
		glfwPollEvents();

		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		glfwGetCursorPos(window, &lastX, &lastY);
	}

	glfwTerminate();

    return 0;
}

void loadTexture(unsigned int &texture, const char* path)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int width, height, nrChannels;
	unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);
	unsigned int externalFormat = nrChannels == 3 ? GL_RGB : GL_RGBA;

	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, width, height, 0, externalFormat, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture\n";
	}

	stbi_image_free(data);
}


