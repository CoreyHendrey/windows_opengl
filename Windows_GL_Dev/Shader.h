#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Material.h"
#include "PointLight.h"


class Shader
{
public:
	unsigned int ID;
	void use();
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;
	void setVec4(const std::string &name, glm::vec4 values) const;
	void setVec3(const std::string &name, glm::vec3 values) const;
	void setMat4fv(const std::string& name, glm::mat4 matrix) const;
	void setMaterial(Material m) const;
	void setPointLight(int id, PointLight l);

private:
	unsigned int createShader(unsigned int shaderType, const char* source);
};

#endif